﻿namespace WCKD_The_Virus
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.timpRamas = new System.Windows.Forms.ProgressBar();
            this.Points = new System.Windows.Forms.ProgressBar();
            this.Life = new System.Windows.Forms.ProgressBar();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.PointsText = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Death20 = new System.Windows.Forms.PictureBox();
            this.Death19 = new System.Windows.Forms.PictureBox();
            this.Death18 = new System.Windows.Forms.PictureBox();
            this.Death17 = new System.Windows.Forms.PictureBox();
            this.Death14 = new System.Windows.Forms.PictureBox();
            this.Death11 = new System.Windows.Forms.PictureBox();
            this.Death15 = new System.Windows.Forms.PictureBox();
            this.Death12 = new System.Windows.Forms.PictureBox();
            this.Death16 = new System.Windows.Forms.PictureBox();
            this.Death13 = new System.Windows.Forms.PictureBox();
            this.Death10 = new System.Windows.Forms.PictureBox();
            this.BoxASUS38 = new System.Windows.Forms.PictureBox();
            this.Death9 = new System.Windows.Forms.PictureBox();
            this.Death7 = new System.Windows.Forms.PictureBox();
            this.Death8 = new System.Windows.Forms.PictureBox();
            this.Death6 = new System.Windows.Forms.PictureBox();
            this.Death5 = new System.Windows.Forms.PictureBox();
            this.Death4 = new System.Windows.Forms.PictureBox();
            this.Death3 = new System.Windows.Forms.PictureBox();
            this.Death2 = new System.Windows.Forms.PictureBox();
            this.Death1 = new System.Windows.Forms.PictureBox();
            this.p27 = new System.Windows.Forms.PictureBox();
            this.p26 = new System.Windows.Forms.PictureBox();
            this.p25 = new System.Windows.Forms.PictureBox();
            this.p24 = new System.Windows.Forms.PictureBox();
            this.p23 = new System.Windows.Forms.PictureBox();
            this.p22 = new System.Windows.Forms.PictureBox();
            this.p21 = new System.Windows.Forms.PictureBox();
            this.p20 = new System.Windows.Forms.PictureBox();
            this.p19 = new System.Windows.Forms.PictureBox();
            this.p18 = new System.Windows.Forms.PictureBox();
            this.p17 = new System.Windows.Forms.PictureBox();
            this.p16 = new System.Windows.Forms.PictureBox();
            this.p15 = new System.Windows.Forms.PictureBox();
            this.p14 = new System.Windows.Forms.PictureBox();
            this.p13 = new System.Windows.Forms.PictureBox();
            this.p12 = new System.Windows.Forms.PictureBox();
            this.p11 = new System.Windows.Forms.PictureBox();
            this.p10 = new System.Windows.Forms.PictureBox();
            this.p9 = new System.Windows.Forms.PictureBox();
            this.p8 = new System.Windows.Forms.PictureBox();
            this.p7 = new System.Windows.Forms.PictureBox();
            this.p6 = new System.Windows.Forms.PictureBox();
            this.p5 = new System.Windows.Forms.PictureBox();
            this.p4 = new System.Windows.Forms.PictureBox();
            this.p3 = new System.Windows.Forms.PictureBox();
            this.BoxASUS34 = new System.Windows.Forms.PictureBox();
            this.BoxASUS32 = new System.Windows.Forms.PictureBox();
            this.BoxASUS36 = new System.Windows.Forms.PictureBox();
            this.BoxASUS37 = new System.Windows.Forms.PictureBox();
            this.BoxASUS26 = new System.Windows.Forms.PictureBox();
            this.BoxASUS35 = new System.Windows.Forms.PictureBox();
            this.BoxASUS33 = new System.Windows.Forms.PictureBox();
            this.BoxASUS31 = new System.Windows.Forms.PictureBox();
            this.BoxASUS30 = new System.Windows.Forms.PictureBox();
            this.BoxASUS29 = new System.Windows.Forms.PictureBox();
            this.BoxASUS28 = new System.Windows.Forms.PictureBox();
            this.BoxASUS27 = new System.Windows.Forms.PictureBox();
            this.BoxASUS25 = new System.Windows.Forms.PictureBox();
            this.BoxASUS24 = new System.Windows.Forms.PictureBox();
            this.BoxASUS23 = new System.Windows.Forms.PictureBox();
            this.BoxASUS22 = new System.Windows.Forms.PictureBox();
            this.p2 = new System.Windows.Forms.PictureBox();
            this.p1 = new System.Windows.Forms.PictureBox();
            this.BoxASUS21 = new System.Windows.Forms.PictureBox();
            this.BoxASUS16 = new System.Windows.Forms.PictureBox();
            this.BoxASUS20 = new System.Windows.Forms.PictureBox();
            this.BoxASUS17 = new System.Windows.Forms.PictureBox();
            this.BoxASUS15 = new System.Windows.Forms.PictureBox();
            this.BoxASUS14 = new System.Windows.Forms.PictureBox();
            this.BoxASUS13 = new System.Windows.Forms.PictureBox();
            this.BoxASUS12 = new System.Windows.Forms.PictureBox();
            this.finish = new System.Windows.Forms.PictureBox();
            this.BoxASUS11 = new System.Windows.Forms.PictureBox();
            this.BoxASUS10 = new System.Windows.Forms.PictureBox();
            this.BoxASUS9 = new System.Windows.Forms.PictureBox();
            this.BoxASUS8 = new System.Windows.Forms.PictureBox();
            this.BoxASUS7 = new System.Windows.Forms.PictureBox();
            this.BoxASUS6 = new System.Windows.Forms.PictureBox();
            this.BoxASUS5 = new System.Windows.Forms.PictureBox();
            this.BoxASUS4 = new System.Windows.Forms.PictureBox();
            this.BoxASUS3 = new System.Windows.Forms.PictureBox();
            this.BoxASUS2 = new System.Windows.Forms.PictureBox();
            this.BoxASUS1 = new System.Windows.Forms.PictureBox();
            this.BorderLeft = new System.Windows.Forms.PictureBox();
            this.BorderBottom = new System.Windows.Forms.PictureBox();
            this.BorderRight = new System.Windows.Forms.PictureBox();
            this.BorderTop = new System.Windows.Forms.PictureBox();
            this.WCKD = new System.Windows.Forms.PictureBox();
            this.MotherBoard2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Death20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WCKD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotherBoard2)).BeginInit();
            this.SuspendLayout();
            // 
            // timpRamas
            // 
            this.timpRamas.ForeColor = System.Drawing.Color.Yellow;
            this.timpRamas.Location = new System.Drawing.Point(38, 748);
            this.timpRamas.Maximum = 60;
            this.timpRamas.Name = "timpRamas";
            this.timpRamas.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.timpRamas.Size = new System.Drawing.Size(173, 28);
            this.timpRamas.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.timpRamas.TabIndex = 40;
            this.timpRamas.Value = 60;
            // 
            // Points
            // 
            this.Points.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.Points.Location = new System.Drawing.Point(217, 748);
            this.Points.Maximum = 200;
            this.Points.Name = "Points";
            this.Points.Size = new System.Drawing.Size(184, 28);
            this.Points.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Points.TabIndex = 41;
            // 
            // Life
            // 
            this.Life.ForeColor = System.Drawing.Color.LimeGreen;
            this.Life.Location = new System.Drawing.Point(407, 748);
            this.Life.Name = "Life";
            this.Life.Size = new System.Drawing.Size(174, 28);
            this.Life.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Life.TabIndex = 42;
            this.Life.Value = 100;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Yellow;
            this.label3.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(83, 751);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 23);
            this.label3.TabIndex = 68;
            this.label3.Text = "Time Left";
            this.label3.Visible = false;
            // 
            // PointsText
            // 
            this.PointsText.AutoSize = true;
            this.PointsText.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.PointsText.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PointsText.Location = new System.Drawing.Point(260, 751);
            this.PointsText.Name = "PointsText";
            this.PointsText.Size = new System.Drawing.Size(99, 23);
            this.PointsText.TabIndex = 69;
            this.PointsText.Text = "PointsToGo";
            this.PointsText.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.LimeGreen;
            this.label2.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(462, 750);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 23);
            this.label2.TabIndex = 70;
            this.label2.Text = "Health";
            this.label2.Visible = false;
            // 
            // Death20
            // 
            this.Death20.BackColor = System.Drawing.Color.Red;
            this.Death20.Location = new System.Drawing.Point(78, 608);
            this.Death20.Name = "Death20";
            this.Death20.Size = new System.Drawing.Size(11, 11);
            this.Death20.TabIndex = 160;
            this.Death20.TabStop = false;
            this.Death20.Visible = false;
            // 
            // Death19
            // 
            this.Death19.BackColor = System.Drawing.Color.Red;
            this.Death19.Location = new System.Drawing.Point(78, 625);
            this.Death19.Name = "Death19";
            this.Death19.Size = new System.Drawing.Size(11, 11);
            this.Death19.TabIndex = 159;
            this.Death19.TabStop = false;
            this.Death19.Visible = false;
            // 
            // Death18
            // 
            this.Death18.BackColor = System.Drawing.Color.Red;
            this.Death18.Location = new System.Drawing.Point(65, 625);
            this.Death18.Name = "Death18";
            this.Death18.Size = new System.Drawing.Size(11, 11);
            this.Death18.TabIndex = 158;
            this.Death18.TabStop = false;
            this.Death18.Visible = false;
            // 
            // Death17
            // 
            this.Death17.BackColor = System.Drawing.Color.Red;
            this.Death17.Location = new System.Drawing.Point(65, 608);
            this.Death17.Name = "Death17";
            this.Death17.Size = new System.Drawing.Size(11, 11);
            this.Death17.TabIndex = 157;
            this.Death17.TabStop = false;
            this.Death17.Visible = false;
            // 
            // Death14
            // 
            this.Death14.BackColor = System.Drawing.Color.Red;
            this.Death14.Location = new System.Drawing.Point(70, 724);
            this.Death14.Name = "Death14";
            this.Death14.Size = new System.Drawing.Size(11, 11);
            this.Death14.TabIndex = 156;
            this.Death14.TabStop = false;
            this.Death14.Visible = false;
            // 
            // Death11
            // 
            this.Death11.BackColor = System.Drawing.Color.Red;
            this.Death11.Location = new System.Drawing.Point(70, 709);
            this.Death11.Name = "Death11";
            this.Death11.Size = new System.Drawing.Size(11, 11);
            this.Death11.TabIndex = 155;
            this.Death11.TabStop = false;
            this.Death11.Visible = false;
            // 
            // Death15
            // 
            this.Death15.BackColor = System.Drawing.Color.Red;
            this.Death15.Location = new System.Drawing.Point(87, 724);
            this.Death15.Name = "Death15";
            this.Death15.Size = new System.Drawing.Size(11, 11);
            this.Death15.TabIndex = 154;
            this.Death15.TabStop = false;
            this.Death15.Visible = false;
            // 
            // Death12
            // 
            this.Death12.BackColor = System.Drawing.Color.Red;
            this.Death12.Location = new System.Drawing.Point(86, 708);
            this.Death12.Name = "Death12";
            this.Death12.Size = new System.Drawing.Size(11, 11);
            this.Death12.TabIndex = 153;
            this.Death12.TabStop = false;
            this.Death12.Visible = false;
            // 
            // Death16
            // 
            this.Death16.BackColor = System.Drawing.Color.Red;
            this.Death16.Location = new System.Drawing.Point(104, 724);
            this.Death16.Name = "Death16";
            this.Death16.Size = new System.Drawing.Size(11, 11);
            this.Death16.TabIndex = 152;
            this.Death16.TabStop = false;
            this.Death16.Visible = false;
            // 
            // Death13
            // 
            this.Death13.BackColor = System.Drawing.Color.Red;
            this.Death13.Location = new System.Drawing.Point(104, 709);
            this.Death13.Name = "Death13";
            this.Death13.Size = new System.Drawing.Size(11, 11);
            this.Death13.TabIndex = 151;
            this.Death13.TabStop = false;
            this.Death13.Visible = false;
            // 
            // Death10
            // 
            this.Death10.BackColor = System.Drawing.Color.Red;
            this.Death10.Location = new System.Drawing.Point(46, 416);
            this.Death10.Name = "Death10";
            this.Death10.Size = new System.Drawing.Size(11, 11);
            this.Death10.TabIndex = 150;
            this.Death10.TabStop = false;
            this.Death10.Visible = false;
            // 
            // BoxASUS38
            // 
            this.BoxASUS38.BackColor = System.Drawing.Color.Red;
            this.BoxASUS38.Location = new System.Drawing.Point(135, 133);
            this.BoxASUS38.Name = "BoxASUS38";
            this.BoxASUS38.Size = new System.Drawing.Size(55, 30);
            this.BoxASUS38.TabIndex = 149;
            this.BoxASUS38.TabStop = false;
            // 
            // Death9
            // 
            this.Death9.BackColor = System.Drawing.Color.Red;
            this.Death9.Location = new System.Drawing.Point(46, 697);
            this.Death9.Name = "Death9";
            this.Death9.Size = new System.Drawing.Size(11, 11);
            this.Death9.TabIndex = 148;
            this.Death9.TabStop = false;
            this.Death9.Visible = false;
            // 
            // Death7
            // 
            this.Death7.BackColor = System.Drawing.Color.Red;
            this.Death7.Location = new System.Drawing.Point(401, 698);
            this.Death7.Name = "Death7";
            this.Death7.Size = new System.Drawing.Size(11, 11);
            this.Death7.TabIndex = 147;
            this.Death7.TabStop = false;
            this.Death7.Visible = false;
            // 
            // Death8
            // 
            this.Death8.BackColor = System.Drawing.Color.Red;
            this.Death8.Location = new System.Drawing.Point(566, 698);
            this.Death8.Name = "Death8";
            this.Death8.Size = new System.Drawing.Size(11, 11);
            this.Death8.TabIndex = 146;
            this.Death8.TabStop = false;
            this.Death8.Visible = false;
            // 
            // Death6
            // 
            this.Death6.BackColor = System.Drawing.Color.Red;
            this.Death6.Location = new System.Drawing.Point(394, 411);
            this.Death6.Name = "Death6";
            this.Death6.Size = new System.Drawing.Size(11, 11);
            this.Death6.TabIndex = 145;
            this.Death6.TabStop = false;
            this.Death6.Visible = false;
            // 
            // Death5
            // 
            this.Death5.BackColor = System.Drawing.Color.Red;
            this.Death5.Location = new System.Drawing.Point(568, 415);
            this.Death5.Name = "Death5";
            this.Death5.Size = new System.Drawing.Size(11, 14);
            this.Death5.TabIndex = 144;
            this.Death5.TabStop = false;
            this.Death5.Visible = false;
            // 
            // Death4
            // 
            this.Death4.BackColor = System.Drawing.Color.Red;
            this.Death4.Location = new System.Drawing.Point(70, 617);
            this.Death4.Name = "Death4";
            this.Death4.Size = new System.Drawing.Size(11, 11);
            this.Death4.TabIndex = 143;
            this.Death4.TabStop = false;
            this.Death4.Visible = false;
            // 
            // Death3
            // 
            this.Death3.BackColor = System.Drawing.Color.Red;
            this.Death3.Location = new System.Drawing.Point(97, 58);
            this.Death3.Name = "Death3";
            this.Death3.Size = new System.Drawing.Size(10, 10);
            this.Death3.TabIndex = 142;
            this.Death3.TabStop = false;
            this.Death3.Visible = false;
            // 
            // Death2
            // 
            this.Death2.BackColor = System.Drawing.Color.Red;
            this.Death2.Location = new System.Drawing.Point(395, 59);
            this.Death2.Name = "Death2";
            this.Death2.Size = new System.Drawing.Size(10, 10);
            this.Death2.TabIndex = 141;
            this.Death2.TabStop = false;
            this.Death2.Visible = false;
            // 
            // Death1
            // 
            this.Death1.BackColor = System.Drawing.Color.Red;
            this.Death1.Location = new System.Drawing.Point(565, 52);
            this.Death1.Name = "Death1";
            this.Death1.Size = new System.Drawing.Size(10, 10);
            this.Death1.TabIndex = 140;
            this.Death1.TabStop = false;
            this.Death1.Visible = false;
            // 
            // p27
            // 
            this.p27.BackColor = System.Drawing.Color.Lime;
            this.p27.Location = new System.Drawing.Point(214, 274);
            this.p27.Name = "p27";
            this.p27.Size = new System.Drawing.Size(10, 10);
            this.p27.TabIndex = 139;
            this.p27.TabStop = false;
            // 
            // p26
            // 
            this.p26.BackColor = System.Drawing.Color.Lime;
            this.p26.Location = new System.Drawing.Point(214, 253);
            this.p26.Name = "p26";
            this.p26.Size = new System.Drawing.Size(10, 10);
            this.p26.TabIndex = 138;
            this.p26.TabStop = false;
            // 
            // p25
            // 
            this.p25.BackColor = System.Drawing.Color.Lime;
            this.p25.Location = new System.Drawing.Point(214, 233);
            this.p25.Name = "p25";
            this.p25.Size = new System.Drawing.Size(10, 10);
            this.p25.TabIndex = 137;
            this.p25.TabStop = false;
            // 
            // p24
            // 
            this.p24.BackColor = System.Drawing.Color.Lime;
            this.p24.Location = new System.Drawing.Point(214, 212);
            this.p24.Name = "p24";
            this.p24.Size = new System.Drawing.Size(10, 10);
            this.p24.TabIndex = 136;
            this.p24.TabStop = false;
            // 
            // p23
            // 
            this.p23.BackColor = System.Drawing.Color.Lime;
            this.p23.Location = new System.Drawing.Point(214, 193);
            this.p23.Name = "p23";
            this.p23.Size = new System.Drawing.Size(10, 10);
            this.p23.TabIndex = 135;
            this.p23.TabStop = false;
            // 
            // p22
            // 
            this.p22.BackColor = System.Drawing.Color.Lime;
            this.p22.Location = new System.Drawing.Point(214, 172);
            this.p22.Name = "p22";
            this.p22.Size = new System.Drawing.Size(10, 10);
            this.p22.TabIndex = 134;
            this.p22.TabStop = false;
            // 
            // p21
            // 
            this.p21.BackColor = System.Drawing.Color.Lime;
            this.p21.Location = new System.Drawing.Point(214, 152);
            this.p21.Name = "p21";
            this.p21.Size = new System.Drawing.Size(10, 10);
            this.p21.TabIndex = 133;
            this.p21.TabStop = false;
            // 
            // p20
            // 
            this.p20.BackColor = System.Drawing.Color.Lime;
            this.p20.Location = new System.Drawing.Point(214, 133);
            this.p20.Name = "p20";
            this.p20.Size = new System.Drawing.Size(10, 10);
            this.p20.TabIndex = 132;
            this.p20.TabStop = false;
            // 
            // p19
            // 
            this.p19.BackColor = System.Drawing.Color.Lime;
            this.p19.Location = new System.Drawing.Point(213, 113);
            this.p19.Name = "p19";
            this.p19.Size = new System.Drawing.Size(10, 10);
            this.p19.TabIndex = 131;
            this.p19.TabStop = false;
            // 
            // p18
            // 
            this.p18.BackColor = System.Drawing.Color.Lime;
            this.p18.Location = new System.Drawing.Point(257, 113);
            this.p18.Name = "p18";
            this.p18.Size = new System.Drawing.Size(10, 10);
            this.p18.TabIndex = 130;
            this.p18.TabStop = false;
            // 
            // p17
            // 
            this.p17.BackColor = System.Drawing.Color.Lime;
            this.p17.Location = new System.Drawing.Point(301, 113);
            this.p17.Name = "p17";
            this.p17.Size = new System.Drawing.Size(10, 10);
            this.p17.TabIndex = 129;
            this.p17.TabStop = false;
            // 
            // p16
            // 
            this.p16.BackColor = System.Drawing.Color.Lime;
            this.p16.Location = new System.Drawing.Point(145, 86);
            this.p16.Name = "p16";
            this.p16.Size = new System.Drawing.Size(10, 10);
            this.p16.TabIndex = 128;
            this.p16.TabStop = false;
            // 
            // p15
            // 
            this.p15.BackColor = System.Drawing.Color.Lime;
            this.p15.Location = new System.Drawing.Point(102, 123);
            this.p15.Name = "p15";
            this.p15.Size = new System.Drawing.Size(10, 10);
            this.p15.TabIndex = 127;
            this.p15.TabStop = false;
            // 
            // p14
            // 
            this.p14.BackColor = System.Drawing.Color.Lime;
            this.p14.Location = new System.Drawing.Point(63, 246);
            this.p14.Name = "p14";
            this.p14.Size = new System.Drawing.Size(10, 10);
            this.p14.TabIndex = 126;
            this.p14.TabStop = false;
            // 
            // p13
            // 
            this.p13.BackColor = System.Drawing.Color.Lime;
            this.p13.Location = new System.Drawing.Point(388, 444);
            this.p13.Name = "p13";
            this.p13.Size = new System.Drawing.Size(10, 10);
            this.p13.TabIndex = 125;
            this.p13.TabStop = false;
            // 
            // p12
            // 
            this.p12.BackColor = System.Drawing.Color.Lime;
            this.p12.Location = new System.Drawing.Point(318, 404);
            this.p12.Name = "p12";
            this.p12.Size = new System.Drawing.Size(10, 10);
            this.p12.TabIndex = 124;
            this.p12.TabStop = false;
            // 
            // p11
            // 
            this.p11.BackColor = System.Drawing.Color.Lime;
            this.p11.Location = new System.Drawing.Point(133, 483);
            this.p11.Name = "p11";
            this.p11.Size = new System.Drawing.Size(10, 10);
            this.p11.TabIndex = 123;
            this.p11.TabStop = false;
            // 
            // p10
            // 
            this.p10.BackColor = System.Drawing.Color.Lime;
            this.p10.Location = new System.Drawing.Point(321, 625);
            this.p10.Name = "p10";
            this.p10.Size = new System.Drawing.Size(10, 10);
            this.p10.TabIndex = 122;
            this.p10.TabStop = false;
            // 
            // p9
            // 
            this.p9.BackColor = System.Drawing.Color.Lime;
            this.p9.Location = new System.Drawing.Point(150, 624);
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(10, 10);
            this.p9.TabIndex = 121;
            this.p9.TabStop = false;
            // 
            // p8
            // 
            this.p8.BackColor = System.Drawing.Color.Lime;
            this.p8.Location = new System.Drawing.Point(302, 404);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(10, 10);
            this.p8.TabIndex = 120;
            this.p8.TabStop = false;
            // 
            // p7
            // 
            this.p7.BackColor = System.Drawing.Color.Lime;
            this.p7.Location = new System.Drawing.Point(487, 446);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(10, 10);
            this.p7.TabIndex = 119;
            this.p7.TabStop = false;
            // 
            // p6
            // 
            this.p6.BackColor = System.Drawing.Color.Lime;
            this.p6.Location = new System.Drawing.Point(501, 430);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(10, 10);
            this.p6.TabIndex = 118;
            this.p6.TabStop = false;
            // 
            // p5
            // 
            this.p5.BackColor = System.Drawing.Color.Lime;
            this.p5.Location = new System.Drawing.Point(501, 445);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(10, 10);
            this.p5.TabIndex = 117;
            this.p5.TabStop = false;
            // 
            // p4
            // 
            this.p4.BackColor = System.Drawing.Color.Lime;
            this.p4.Location = new System.Drawing.Point(528, 290);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(10, 10);
            this.p4.TabIndex = 116;
            this.p4.TabStop = false;
            // 
            // p3
            // 
            this.p3.BackColor = System.Drawing.Color.Lime;
            this.p3.Location = new System.Drawing.Point(528, 274);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(10, 10);
            this.p3.TabIndex = 115;
            this.p3.TabStop = false;
            // 
            // BoxASUS34
            // 
            this.BoxASUS34.BackColor = System.Drawing.Color.Red;
            this.BoxASUS34.Location = new System.Drawing.Point(346, 406);
            this.BoxASUS34.Name = "BoxASUS34";
            this.BoxASUS34.Size = new System.Drawing.Size(10, 21);
            this.BoxASUS34.TabIndex = 114;
            this.BoxASUS34.TabStop = false;
            // 
            // BoxASUS32
            // 
            this.BoxASUS32.BackColor = System.Drawing.Color.Red;
            this.BoxASUS32.Location = new System.Drawing.Point(66, 474);
            this.BoxASUS32.Name = "BoxASUS32";
            this.BoxASUS32.Size = new System.Drawing.Size(13, 19);
            this.BoxASUS32.TabIndex = 113;
            this.BoxASUS32.TabStop = false;
            // 
            // BoxASUS36
            // 
            this.BoxASUS36.BackColor = System.Drawing.Color.Red;
            this.BoxASUS36.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS36.Location = new System.Drawing.Point(133, 212);
            this.BoxASUS36.Name = "BoxASUS36";
            this.BoxASUS36.Size = new System.Drawing.Size(29, 20);
            this.BoxASUS36.TabIndex = 112;
            this.BoxASUS36.TabStop = false;
            // 
            // BoxASUS37
            // 
            this.BoxASUS37.BackColor = System.Drawing.Color.Red;
            this.BoxASUS37.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS37.Location = new System.Drawing.Point(130, 152);
            this.BoxASUS37.Name = "BoxASUS37";
            this.BoxASUS37.Size = new System.Drawing.Size(48, 67);
            this.BoxASUS37.TabIndex = 111;
            this.BoxASUS37.TabStop = false;
            // 
            // BoxASUS26
            // 
            this.BoxASUS26.BackColor = System.Drawing.Color.Red;
            this.BoxASUS26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS26.Location = new System.Drawing.Point(182, 151);
            this.BoxASUS26.Name = "BoxASUS26";
            this.BoxASUS26.Size = new System.Drawing.Size(18, 161);
            this.BoxASUS26.TabIndex = 110;
            this.BoxASUS26.TabStop = false;
            // 
            // BoxASUS35
            // 
            this.BoxASUS35.BackColor = System.Drawing.Color.Red;
            this.BoxASUS35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS35.Location = new System.Drawing.Point(150, 306);
            this.BoxASUS35.Name = "BoxASUS35";
            this.BoxASUS35.Size = new System.Drawing.Size(40, 40);
            this.BoxASUS35.TabIndex = 109;
            this.BoxASUS35.TabStop = false;
            this.BoxASUS35.Visible = false;
            // 
            // BoxASUS33
            // 
            this.BoxASUS33.BackColor = System.Drawing.Color.Red;
            this.BoxASUS33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS33.Location = new System.Drawing.Point(112, 231);
            this.BoxASUS33.Name = "BoxASUS33";
            this.BoxASUS33.Size = new System.Drawing.Size(44, 37);
            this.BoxASUS33.TabIndex = 108;
            this.BoxASUS33.TabStop = false;
            // 
            // BoxASUS31
            // 
            this.BoxASUS31.BackColor = System.Drawing.Color.Red;
            this.BoxASUS31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS31.Location = new System.Drawing.Point(120, 263);
            this.BoxASUS31.Name = "BoxASUS31";
            this.BoxASUS31.Size = new System.Drawing.Size(34, 37);
            this.BoxASUS31.TabIndex = 107;
            this.BoxASUS31.TabStop = false;
            // 
            // BoxASUS30
            // 
            this.BoxASUS30.BackColor = System.Drawing.Color.Red;
            this.BoxASUS30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS30.Location = new System.Drawing.Point(129, 299);
            this.BoxASUS30.Name = "BoxASUS30";
            this.BoxASUS30.Size = new System.Drawing.Size(34, 37);
            this.BoxASUS30.TabIndex = 106;
            this.BoxASUS30.TabStop = false;
            // 
            // BoxASUS29
            // 
            this.BoxASUS29.BackColor = System.Drawing.Color.Red;
            this.BoxASUS29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS29.Location = new System.Drawing.Point(102, 162);
            this.BoxASUS29.Name = "BoxASUS29";
            this.BoxASUS29.Size = new System.Drawing.Size(37, 95);
            this.BoxASUS29.TabIndex = 105;
            this.BoxASUS29.TabStop = false;
            // 
            // BoxASUS28
            // 
            this.BoxASUS28.BackColor = System.Drawing.Color.Red;
            this.BoxASUS28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS28.Location = new System.Drawing.Point(170, 59);
            this.BoxASUS28.Name = "BoxASUS28";
            this.BoxASUS28.Size = new System.Drawing.Size(198, 29);
            this.BoxASUS28.TabIndex = 104;
            this.BoxASUS28.TabStop = false;
            // 
            // BoxASUS27
            // 
            this.BoxASUS27.BackColor = System.Drawing.Color.Red;
            this.BoxASUS27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS27.Location = new System.Drawing.Point(182, 80);
            this.BoxASUS27.Name = "BoxASUS27";
            this.BoxASUS27.Size = new System.Drawing.Size(177, 26);
            this.BoxASUS27.TabIndex = 103;
            this.BoxASUS27.TabStop = false;
            // 
            // BoxASUS25
            // 
            this.BoxASUS25.BackColor = System.Drawing.Color.Red;
            this.BoxASUS25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS25.Location = new System.Drawing.Point(150, 51);
            this.BoxASUS25.Name = "BoxASUS25";
            this.BoxASUS25.Size = new System.Drawing.Size(230, 18);
            this.BoxASUS25.TabIndex = 102;
            this.BoxASUS25.TabStop = false;
            // 
            // BoxASUS24
            // 
            this.BoxASUS24.BackColor = System.Drawing.Color.Red;
            this.BoxASUS24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS24.Location = new System.Drawing.Point(150, 172);
            this.BoxASUS24.Name = "BoxASUS24";
            this.BoxASUS24.Size = new System.Drawing.Size(40, 140);
            this.BoxASUS24.TabIndex = 101;
            this.BoxASUS24.TabStop = false;
            // 
            // BoxASUS23
            // 
            this.BoxASUS23.BackColor = System.Drawing.Color.Red;
            this.BoxASUS23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS23.Location = new System.Drawing.Point(267, 148);
            this.BoxASUS23.Name = "BoxASUS23";
            this.BoxASUS23.Size = new System.Drawing.Size(112, 15);
            this.BoxASUS23.TabIndex = 100;
            this.BoxASUS23.TabStop = false;
            // 
            // BoxASUS22
            // 
            this.BoxASUS22.BackColor = System.Drawing.Color.Red;
            this.BoxASUS22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS22.Location = new System.Drawing.Point(270, 347);
            this.BoxASUS22.Name = "BoxASUS22";
            this.BoxASUS22.Size = new System.Drawing.Size(98, 13);
            this.BoxASUS22.TabIndex = 99;
            this.BoxASUS22.TabStop = false;
            // 
            // p2
            // 
            this.p2.BackColor = System.Drawing.Color.Lime;
            this.p2.Location = new System.Drawing.Point(527, 258);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(10, 10);
            this.p2.TabIndex = 98;
            this.p2.TabStop = false;
            // 
            // p1
            // 
            this.p1.BackColor = System.Drawing.Color.Lime;
            this.p1.Location = new System.Drawing.Point(527, 242);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(10, 10);
            this.p1.TabIndex = 97;
            this.p1.TabStop = false;
            // 
            // BoxASUS21
            // 
            this.BoxASUS21.BackColor = System.Drawing.Color.Red;
            this.BoxASUS21.Location = new System.Drawing.Point(131, 412);
            this.BoxASUS21.Name = "BoxASUS21";
            this.BoxASUS21.Size = new System.Drawing.Size(46, 13);
            this.BoxASUS21.TabIndex = 96;
            this.BoxASUS21.TabStop = false;
            // 
            // BoxASUS16
            // 
            this.BoxASUS16.BackColor = System.Drawing.Color.Red;
            this.BoxASUS16.Location = new System.Drawing.Point(75, 432);
            this.BoxASUS16.Name = "BoxASUS16";
            this.BoxASUS16.Size = new System.Drawing.Size(10, 16);
            this.BoxASUS16.TabIndex = 95;
            this.BoxASUS16.TabStop = false;
            // 
            // BoxASUS20
            // 
            this.BoxASUS20.BackColor = System.Drawing.Color.Red;
            this.BoxASUS20.Location = new System.Drawing.Point(61, 550);
            this.BoxASUS20.Name = "BoxASUS20";
            this.BoxASUS20.Size = new System.Drawing.Size(21, 32);
            this.BoxASUS20.TabIndex = 94;
            this.BoxASUS20.TabStop = false;
            // 
            // BoxASUS17
            // 
            this.BoxASUS17.BackColor = System.Drawing.Color.Red;
            this.BoxASUS17.Location = new System.Drawing.Point(210, 495);
            this.BoxASUS17.Name = "BoxASUS17";
            this.BoxASUS17.Size = new System.Drawing.Size(10, 22);
            this.BoxASUS17.TabIndex = 91;
            this.BoxASUS17.TabStop = false;
            // 
            // BoxASUS15
            // 
            this.BoxASUS15.BackColor = System.Drawing.Color.Red;
            this.BoxASUS15.Location = new System.Drawing.Point(443, 651);
            this.BoxASUS15.Name = "BoxASUS15";
            this.BoxASUS15.Size = new System.Drawing.Size(74, 17);
            this.BoxASUS15.TabIndex = 90;
            this.BoxASUS15.TabStop = false;
            // 
            // BoxASUS14
            // 
            this.BoxASUS14.BackColor = System.Drawing.Color.Red;
            this.BoxASUS14.Location = new System.Drawing.Point(471, 641);
            this.BoxASUS14.Name = "BoxASUS14";
            this.BoxASUS14.Size = new System.Drawing.Size(46, 17);
            this.BoxASUS14.TabIndex = 89;
            this.BoxASUS14.TabStop = false;
            // 
            // BoxASUS13
            // 
            this.BoxASUS13.BackColor = System.Drawing.Color.Red;
            this.BoxASUS13.Location = new System.Drawing.Point(417, 641);
            this.BoxASUS13.Name = "BoxASUS13";
            this.BoxASUS13.Size = new System.Drawing.Size(62, 15);
            this.BoxASUS13.TabIndex = 88;
            this.BoxASUS13.TabStop = false;
            // 
            // BoxASUS12
            // 
            this.BoxASUS12.BackColor = System.Drawing.Color.Red;
            this.BoxASUS12.Location = new System.Drawing.Point(417, 461);
            this.BoxASUS12.Name = "BoxASUS12";
            this.BoxASUS12.Size = new System.Drawing.Size(130, 184);
            this.BoxASUS12.TabIndex = 87;
            this.BoxASUS12.TabStop = false;
            // 
            // finish
            // 
            this.finish.BackColor = System.Drawing.Color.Red;
            this.finish.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.finish.Location = new System.Drawing.Point(228, 404);
            this.finish.Name = "finish";
            this.finish.Size = new System.Drawing.Size(39, 42);
            this.finish.TabIndex = 86;
            this.finish.TabStop = false;
            this.finish.Visible = false;
            // 
            // BoxASUS11
            // 
            this.BoxASUS11.BackColor = System.Drawing.Color.Red;
            this.BoxASUS11.Location = new System.Drawing.Point(563, 362);
            this.BoxASUS11.Name = "BoxASUS11";
            this.BoxASUS11.Size = new System.Drawing.Size(10, 42);
            this.BoxASUS11.TabIndex = 85;
            this.BoxASUS11.TabStop = false;
            // 
            // BoxASUS10
            // 
            this.BoxASUS10.BackColor = System.Drawing.Color.Red;
            this.BoxASUS10.Location = new System.Drawing.Point(555, 229);
            this.BoxASUS10.Name = "BoxASUS10";
            this.BoxASUS10.Size = new System.Drawing.Size(16, 107);
            this.BoxASUS10.TabIndex = 84;
            this.BoxASUS10.TabStop = false;
            // 
            // BoxASUS9
            // 
            this.BoxASUS9.BackColor = System.Drawing.Color.Red;
            this.BoxASUS9.Location = new System.Drawing.Point(445, 96);
            this.BoxASUS9.Name = "BoxASUS9";
            this.BoxASUS9.Size = new System.Drawing.Size(63, 311);
            this.BoxASUS9.TabIndex = 83;
            this.BoxASUS9.TabStop = false;
            // 
            // BoxASUS8
            // 
            this.BoxASUS8.BackColor = System.Drawing.Color.Red;
            this.BoxASUS8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BoxASUS8.Location = new System.Drawing.Point(286, 212);
            this.BoxASUS8.Name = "BoxASUS8";
            this.BoxASUS8.Size = new System.Drawing.Size(93, 88);
            this.BoxASUS8.TabIndex = 82;
            this.BoxASUS8.TabStop = false;
            // 
            // BoxASUS7
            // 
            this.BoxASUS7.BackColor = System.Drawing.Color.Red;
            this.BoxASUS7.Location = new System.Drawing.Point(135, 691);
            this.BoxASUS7.Name = "BoxASUS7";
            this.BoxASUS7.Size = new System.Drawing.Size(214, 10);
            this.BoxASUS7.TabIndex = 81;
            this.BoxASUS7.TabStop = false;
            // 
            // BoxASUS6
            // 
            this.BoxASUS6.BackColor = System.Drawing.Color.Red;
            this.BoxASUS6.Location = new System.Drawing.Point(122, 646);
            this.BoxASUS6.Name = "BoxASUS6";
            this.BoxASUS6.Size = new System.Drawing.Size(184, 10);
            this.BoxASUS6.TabIndex = 80;
            this.BoxASUS6.TabStop = false;
            // 
            // BoxASUS5
            // 
            this.BoxASUS5.BackColor = System.Drawing.Color.Red;
            this.BoxASUS5.Location = new System.Drawing.Point(135, 507);
            this.BoxASUS5.Name = "BoxASUS5";
            this.BoxASUS5.Size = new System.Drawing.Size(46, 10);
            this.BoxASUS5.TabIndex = 79;
            this.BoxASUS5.TabStop = false;
            // 
            // BoxASUS4
            // 
            this.BoxASUS4.BackColor = System.Drawing.Color.Red;
            this.BoxASUS4.Location = new System.Drawing.Point(126, 551);
            this.BoxASUS4.Name = "BoxASUS4";
            this.BoxASUS4.Size = new System.Drawing.Size(176, 10);
            this.BoxASUS4.TabIndex = 78;
            this.BoxASUS4.TabStop = false;
            // 
            // BoxASUS3
            // 
            this.BoxASUS3.BackColor = System.Drawing.Color.Red;
            this.BoxASUS3.Location = new System.Drawing.Point(134, 599);
            this.BoxASUS3.Name = "BoxASUS3";
            this.BoxASUS3.Size = new System.Drawing.Size(214, 10);
            this.BoxASUS3.TabIndex = 77;
            this.BoxASUS3.TabStop = false;
            // 
            // BoxASUS2
            // 
            this.BoxASUS2.BackColor = System.Drawing.Color.Red;
            this.BoxASUS2.Location = new System.Drawing.Point(135, 458);
            this.BoxASUS2.Name = "BoxASUS2";
            this.BoxASUS2.Size = new System.Drawing.Size(214, 10);
            this.BoxASUS2.TabIndex = 76;
            this.BoxASUS2.TabStop = false;
            // 
            // BoxASUS1
            // 
            this.BoxASUS1.BackColor = System.Drawing.Color.Red;
            this.BoxASUS1.Location = new System.Drawing.Point(150, 123);
            this.BoxASUS1.Name = "BoxASUS1";
            this.BoxASUS1.Size = new System.Drawing.Size(18, 15);
            this.BoxASUS1.TabIndex = 75;
            this.BoxASUS1.TabStop = false;
            // 
            // BorderLeft
            // 
            this.BorderLeft.BackColor = System.Drawing.Color.Yellow;
            this.BorderLeft.Location = new System.Drawing.Point(3, 7);
            this.BorderLeft.Name = "BorderLeft";
            this.BorderLeft.Size = new System.Drawing.Size(34, 735);
            this.BorderLeft.TabIndex = 74;
            this.BorderLeft.TabStop = false;
            this.BorderLeft.Visible = false;
            // 
            // BorderBottom
            // 
            this.BorderBottom.BackColor = System.Drawing.Color.Yellow;
            this.BorderBottom.Location = new System.Drawing.Point(38, 734);
            this.BorderBottom.Name = "BorderBottom";
            this.BorderBottom.Size = new System.Drawing.Size(543, 8);
            this.BorderBottom.TabIndex = 73;
            this.BorderBottom.TabStop = false;
            this.BorderBottom.Visible = false;
            // 
            // BorderRight
            // 
            this.BorderRight.BackColor = System.Drawing.Color.Yellow;
            this.BorderRight.Location = new System.Drawing.Point(587, 24);
            this.BorderRight.Name = "BorderRight";
            this.BorderRight.Size = new System.Drawing.Size(20, 718);
            this.BorderRight.TabIndex = 72;
            this.BorderRight.TabStop = false;
            this.BorderRight.Visible = false;
            // 
            // BorderTop
            // 
            this.BorderTop.BackColor = System.Drawing.Color.Yellow;
            this.BorderTop.Location = new System.Drawing.Point(3, 3);
            this.BorderTop.Name = "BorderTop";
            this.BorderTop.Size = new System.Drawing.Size(621, 42);
            this.BorderTop.TabIndex = 71;
            this.BorderTop.TabStop = false;
            this.BorderTop.Visible = false;
            // 
            // WCKD
            // 
            this.WCKD.BackColor = System.Drawing.Color.Transparent;
            this.WCKD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.WCKD.Image = global::WCKD_The_Virus.Properties.Resources._74730ffcb962d9994a17ea2ee19d7a3a3;
            this.WCKD.Location = new System.Drawing.Point(556, 76);
            this.WCKD.Margin = new System.Windows.Forms.Padding(0);
            this.WCKD.Name = "WCKD";
            this.WCKD.Size = new System.Drawing.Size(25, 24);
            this.WCKD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.WCKD.TabIndex = 43;
            this.WCKD.TabStop = false;
            // 
            // MotherBoard2
            // 
            this.MotherBoard2.BackColor = System.Drawing.Color.Black;
            this.MotherBoard2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MotherBoard2.Image = global::WCKD_The_Virus.Properties.Resources.MotherBoard_22;
            this.MotherBoard2.Location = new System.Drawing.Point(8, 10);
            this.MotherBoard2.Margin = new System.Windows.Forms.Padding(2);
            this.MotherBoard2.Name = "MotherBoard2";
            this.MotherBoard2.Size = new System.Drawing.Size(587, 764);
            this.MotherBoard2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.MotherBoard2.TabIndex = 0;
            this.MotherBoard2.TabStop = false;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(606, 785);
            this.Controls.Add(this.Death20);
            this.Controls.Add(this.Death19);
            this.Controls.Add(this.Death18);
            this.Controls.Add(this.Death17);
            this.Controls.Add(this.Death14);
            this.Controls.Add(this.Death11);
            this.Controls.Add(this.Death15);
            this.Controls.Add(this.Death12);
            this.Controls.Add(this.Death16);
            this.Controls.Add(this.Death13);
            this.Controls.Add(this.Death10);
            this.Controls.Add(this.BoxASUS38);
            this.Controls.Add(this.Death9);
            this.Controls.Add(this.Death7);
            this.Controls.Add(this.Death8);
            this.Controls.Add(this.Death6);
            this.Controls.Add(this.Death5);
            this.Controls.Add(this.Death4);
            this.Controls.Add(this.Death3);
            this.Controls.Add(this.Death2);
            this.Controls.Add(this.Death1);
            this.Controls.Add(this.p27);
            this.Controls.Add(this.p26);
            this.Controls.Add(this.p25);
            this.Controls.Add(this.p24);
            this.Controls.Add(this.p23);
            this.Controls.Add(this.p22);
            this.Controls.Add(this.p21);
            this.Controls.Add(this.p20);
            this.Controls.Add(this.p19);
            this.Controls.Add(this.p18);
            this.Controls.Add(this.p17);
            this.Controls.Add(this.p16);
            this.Controls.Add(this.p15);
            this.Controls.Add(this.p14);
            this.Controls.Add(this.p13);
            this.Controls.Add(this.p12);
            this.Controls.Add(this.p11);
            this.Controls.Add(this.p10);
            this.Controls.Add(this.p9);
            this.Controls.Add(this.p8);
            this.Controls.Add(this.p7);
            this.Controls.Add(this.p6);
            this.Controls.Add(this.p5);
            this.Controls.Add(this.p4);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.BoxASUS34);
            this.Controls.Add(this.BoxASUS32);
            this.Controls.Add(this.BoxASUS36);
            this.Controls.Add(this.BoxASUS37);
            this.Controls.Add(this.BoxASUS26);
            this.Controls.Add(this.BoxASUS35);
            this.Controls.Add(this.BoxASUS33);
            this.Controls.Add(this.BoxASUS31);
            this.Controls.Add(this.BoxASUS30);
            this.Controls.Add(this.BoxASUS29);
            this.Controls.Add(this.BoxASUS28);
            this.Controls.Add(this.BoxASUS27);
            this.Controls.Add(this.BoxASUS25);
            this.Controls.Add(this.BoxASUS24);
            this.Controls.Add(this.BoxASUS23);
            this.Controls.Add(this.BoxASUS22);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.p1);
            this.Controls.Add(this.BoxASUS21);
            this.Controls.Add(this.BoxASUS16);
            this.Controls.Add(this.BoxASUS20);
            this.Controls.Add(this.BoxASUS17);
            this.Controls.Add(this.BoxASUS15);
            this.Controls.Add(this.BoxASUS14);
            this.Controls.Add(this.BoxASUS13);
            this.Controls.Add(this.BoxASUS12);
            this.Controls.Add(this.finish);
            this.Controls.Add(this.BoxASUS11);
            this.Controls.Add(this.BoxASUS10);
            this.Controls.Add(this.BoxASUS9);
            this.Controls.Add(this.BoxASUS8);
            this.Controls.Add(this.BoxASUS7);
            this.Controls.Add(this.BoxASUS6);
            this.Controls.Add(this.BoxASUS5);
            this.Controls.Add(this.BoxASUS4);
            this.Controls.Add(this.BoxASUS3);
            this.Controls.Add(this.BoxASUS2);
            this.Controls.Add(this.BoxASUS1);
            this.Controls.Add(this.BorderLeft);
            this.Controls.Add(this.BorderBottom);
            this.Controls.Add(this.BorderRight);
            this.Controls.Add(this.BorderTop);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PointsText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.WCKD);
            this.Controls.Add(this.Life);
            this.Controls.Add(this.Points);
            this.Controls.Add(this.timpRamas);
            this.Controls.Add(this.MotherBoard2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.Text = "ASUS Map LVL2";
            ((System.ComponentModel.ISupportInitialize)(this.Death20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Death1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoxASUS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BorderTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WCKD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MotherBoard2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox MotherBoard2;
        private System.Windows.Forms.ProgressBar timpRamas;
        private System.Windows.Forms.ProgressBar Points;
        private System.Windows.Forms.ProgressBar Life;
        private System.Windows.Forms.PictureBox WCKD;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label PointsText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox BorderTop;
        private System.Windows.Forms.PictureBox BorderRight;
        private System.Windows.Forms.PictureBox BorderBottom;
        private System.Windows.Forms.PictureBox BorderLeft;
        private System.Windows.Forms.PictureBox BoxASUS1;
        private System.Windows.Forms.PictureBox BoxASUS2;
        private System.Windows.Forms.PictureBox BoxASUS3;
        private System.Windows.Forms.PictureBox BoxASUS4;
        private System.Windows.Forms.PictureBox BoxASUS5;
        private System.Windows.Forms.PictureBox BoxASUS6;
        private System.Windows.Forms.PictureBox BoxASUS7;
        private System.Windows.Forms.PictureBox BoxASUS8;
        private System.Windows.Forms.PictureBox BoxASUS9;
        private System.Windows.Forms.PictureBox BoxASUS10;
        private System.Windows.Forms.PictureBox BoxASUS11;
        private System.Windows.Forms.PictureBox finish;
        private System.Windows.Forms.PictureBox BoxASUS12;
        private System.Windows.Forms.PictureBox BoxASUS13;
        private System.Windows.Forms.PictureBox BoxASUS14;
        private System.Windows.Forms.PictureBox BoxASUS15;
        private System.Windows.Forms.PictureBox BoxASUS17;
        private System.Windows.Forms.PictureBox BoxASUS20;
        private System.Windows.Forms.PictureBox BoxASUS16;
        private System.Windows.Forms.PictureBox BoxASUS21;
        private System.Windows.Forms.PictureBox p1;
        private System.Windows.Forms.PictureBox p2;
        private System.Windows.Forms.PictureBox BoxASUS22;
        private System.Windows.Forms.PictureBox BoxASUS23;
        private System.Windows.Forms.PictureBox BoxASUS24;
        private System.Windows.Forms.PictureBox BoxASUS25;
        private System.Windows.Forms.PictureBox BoxASUS27;
        private System.Windows.Forms.PictureBox BoxASUS28;
        private System.Windows.Forms.PictureBox BoxASUS29;
        private System.Windows.Forms.PictureBox BoxASUS30;
        private System.Windows.Forms.PictureBox BoxASUS31;
        private System.Windows.Forms.PictureBox BoxASUS33;
        private System.Windows.Forms.PictureBox BoxASUS35;
        private System.Windows.Forms.PictureBox BoxASUS26;
        private System.Windows.Forms.PictureBox BoxASUS37;
        private System.Windows.Forms.PictureBox BoxASUS36;
        private System.Windows.Forms.PictureBox BoxASUS32;
        private System.Windows.Forms.PictureBox BoxASUS34;
        private System.Windows.Forms.PictureBox p3;
        private System.Windows.Forms.PictureBox p4;
        private System.Windows.Forms.PictureBox p5;
        private System.Windows.Forms.PictureBox p6;
        private System.Windows.Forms.PictureBox p7;
        private System.Windows.Forms.PictureBox p8;
        private System.Windows.Forms.PictureBox p9;
        private System.Windows.Forms.PictureBox p10;
        private System.Windows.Forms.PictureBox p11;
        private System.Windows.Forms.PictureBox p12;
        private System.Windows.Forms.PictureBox p13;
        private System.Windows.Forms.PictureBox p14;
        private System.Windows.Forms.PictureBox p15;
        private System.Windows.Forms.PictureBox p16;
        private System.Windows.Forms.PictureBox p17;
        private System.Windows.Forms.PictureBox p18;
        private System.Windows.Forms.PictureBox p19;
        private System.Windows.Forms.PictureBox p20;
        private System.Windows.Forms.PictureBox p21;
        private System.Windows.Forms.PictureBox p22;
        private System.Windows.Forms.PictureBox p23;
        private System.Windows.Forms.PictureBox p24;
        private System.Windows.Forms.PictureBox p25;
        private System.Windows.Forms.PictureBox p26;
        private System.Windows.Forms.PictureBox p27;
        private System.Windows.Forms.PictureBox Death1;
        private System.Windows.Forms.PictureBox Death2;
        private System.Windows.Forms.PictureBox Death3;
        private System.Windows.Forms.PictureBox Death5;
        private System.Windows.Forms.PictureBox Death6;
        private System.Windows.Forms.PictureBox Death8;
        private System.Windows.Forms.PictureBox Death7;
        private System.Windows.Forms.PictureBox Death9;
        private System.Windows.Forms.PictureBox BoxASUS38;
        private System.Windows.Forms.PictureBox Death10;
        private System.Windows.Forms.PictureBox Death13;
        private System.Windows.Forms.PictureBox Death16;
        private System.Windows.Forms.PictureBox Death12;
        private System.Windows.Forms.PictureBox Death15;
        private System.Windows.Forms.PictureBox Death11;
        private System.Windows.Forms.PictureBox Death14;
        private System.Windows.Forms.PictureBox Death17;
        private System.Windows.Forms.PictureBox Death18;
        private System.Windows.Forms.PictureBox Death19;
        private System.Windows.Forms.PictureBox Death20;
        private System.Windows.Forms.PictureBox Death4;
    }
}