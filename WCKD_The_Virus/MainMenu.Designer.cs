﻿namespace WCKD_The_Virus
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.startButton = new System.Windows.Forms.Button();
            this.Exit_Button = new System.Windows.Forms.Button();
            this.Rules_Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.BackColor = System.Drawing.Color.Transparent;
            this.startButton.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.startButton.FlatAppearance.BorderSize = 0;
            this.startButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.startButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.startButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startButton.ForeColor = System.Drawing.Color.Transparent;
            this.startButton.Location = new System.Drawing.Point(246, 582);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(165, 161);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "button1";
            this.startButton.UseVisualStyleBackColor = false;
            this.startButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickStart);
            // 
            // Exit_Button
            // 
            this.Exit_Button.BackColor = System.Drawing.Color.Transparent;
            this.Exit_Button.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.Exit_Button.FlatAppearance.BorderSize = 0;
            this.Exit_Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Exit_Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Exit_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit_Button.ForeColor = System.Drawing.Color.Transparent;
            this.Exit_Button.Location = new System.Drawing.Point(453, 582);
            this.Exit_Button.Name = "Exit_Button";
            this.Exit_Button.Size = new System.Drawing.Size(165, 161);
            this.Exit_Button.TabIndex = 1;
            this.Exit_Button.UseVisualStyleBackColor = false;
            this.Exit_Button.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickExit);
            // 
            // Rules_Button
            // 
            this.Rules_Button.BackColor = System.Drawing.Color.Transparent;
            this.Rules_Button.FlatAppearance.BorderColor = System.Drawing.Color.DarkRed;
            this.Rules_Button.FlatAppearance.BorderSize = 0;
            this.Rules_Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Rules_Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Rules_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Rules_Button.ForeColor = System.Drawing.Color.Transparent;
            this.Rules_Button.Location = new System.Drawing.Point(26, 582);
            this.Rules_Button.Name = "Rules_Button";
            this.Rules_Button.Size = new System.Drawing.Size(165, 161);
            this.Rules_Button.TabIndex = 2;
            this.Rules_Button.UseVisualStyleBackColor = false;
            this.Rules_Button.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickRules);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::WCKD_The_Virus.Properties.Resources.Game_Wallpaper4;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(654, 773);
            this.Controls.Add(this.Rules_Button);
            this.Controls.Add(this.Exit_Button);
            this.Controls.Add(this.startButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainMenu";
            this.Text = "WCKD - The Virus";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button Exit_Button;
        private System.Windows.Forms.Button Rules_Button;
    }
}