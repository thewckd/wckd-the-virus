﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace WCKD_The_Virus
{
    public partial class Form1 : Form
    {

        int timeLeft = 1;
        int lvlComplete = 0;
        List<int> visited = new List<int>(30);

        public void setVisited()
        {
            for (int i = 0; i <= 25; i++)
            {
                visited.Insert(i, 0);
            }
        }
        public Form1()
        {
            InitializeComponent();
            setVisited();
            int health = 100;
            timer.Start();
            timer.Interval = 1000;
            MotherBoard1.Controls.Add(WCKD);
            WCKD.BackColor = Color.Transparent;
            this.KeyDown += new KeyEventHandler(this.Game_KeyDown);
            this.KeyDown += new KeyEventHandler(this.Collisions);
        }

        private void Game_KeyDown(object sender, KeyEventArgs key)
        {
            if (key.KeyCode == Keys.Up)
            {
                WCKD.Top -= 5;
            }
            else if (key.KeyCode == Keys.Down)
            {
                WCKD.Top += 5;
            }
            else if (key.KeyCode == Keys.Left)
            {
                WCKD.Left -= 5;
            }
            else if (key.KeyCode == Keys.Right)
            {
                WCKD.Left += 5;
            }
            else if(key.KeyCode == Keys.Escape)
            {
                timer.Stop();
                this.Hide();
                Maps open_game_maps = new Maps();
                open_game_maps.Closed += (s, args) => this.Close();
                open_game_maps.Show();
            }
        }

        public void Collisions(object sender, KeyEventArgs key)
        {
            #region boundsIntersection
            if (WCKD.Bounds.IntersectsWith(BoxCPU.Bounds) || WCKD.Bounds.IntersectsWith(BoxRam.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxTransistor1.Bounds) || WCKD.Bounds.IntersectsWith(BoxMSI.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxMSI2.Bounds) || WCKD.Bounds.IntersectsWith(BoxMSI3.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxMSI4.Bounds) || WCKD.Bounds.IntersectsWith(BoxMSI5.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxMSI6.Bounds) || WCKD.Bounds.IntersectsWith(BoxMSI7.Bounds) ||
                WCKD.Bounds.IntersectsWith(BorderLeft.Bounds) || WCKD.Bounds.IntersectsWith(BorderRight.Bounds) ||
                WCKD.Bounds.IntersectsWith(BorderTop.Bounds) || WCKD.Bounds.IntersectsWith(BorderBottom.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxTransistor2.Bounds) || WCKD.Bounds.IntersectsWith(BoxTransistor3.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxTransistor5.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxTransistor6.Bounds) || WCKD.Bounds.IntersectsWith(BoxPCI1.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxPCI2.Bounds) || WCKD.Bounds.IntersectsWith(BoxPCI3.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxPCI4.Bounds) || WCKD.Bounds.IntersectsWith(BoxPCI5.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxPCI6.Bounds) || WCKD.Bounds.IntersectsWith(BorderLeft3.Bounds) ||
                WCKD.Bounds.IntersectsWith(BoxSSD.Bounds) || WCKD.Bounds.IntersectsWith(pictureBox15.Bounds))

            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\collision.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                if (Life.Value >= 3)
                {
                    Life.Value -= 3;
                }
                else
                {
                    System.Media.SoundPlayer playerMusic1 = new System.Media.SoundPlayer();
                    string fullPathToSound1 = @"..\..\..\Sounds\GameOver.wav";
                    playerMusic.SoundLocation = fullPathToSound1;
                    playerMusic.Load();
                    playerMusic.Play();
                    timer.Stop();
                    this.Hide();
                    gameOverForm open_game_form = new gameOverForm();
                    open_game_form.setScore(Points.Value);
                    open_game_form.setTime(timpRamas.Value);
                    open_game_form.setHealth(Life.Value);
                    open_game_form.Closed += (s, args) => this.Close();
                    open_game_form.Show();
                }

                if (key.KeyCode == Keys.Up)
                {
                    WCKD.Top += 5;

                }
                else if (key.KeyCode == Keys.Down)
                {
                    WCKD.Top -= 5;
                }
                else if (key.KeyCode == Keys.Left)
                {
                    WCKD.Left += 5;
                }
                else if (key.KeyCode == Keys.Right)
                {
                    WCKD.Left -= 5;
                }
            }
            #endregion
            #region pointsIntersection
            else if (WCKD.Bounds.IntersectsWith(p1.Bounds))
            {
                if (visited[1] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p1.Visible = false;
                    visited[1]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p2.Bounds))
            {
                if (visited[2] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p2.Visible = false;
                    visited[2]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p3.Bounds))
            {
                if (visited[3] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p3.Visible = false;
                    visited[3]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p4.Bounds))
            {
                if (visited[4] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p4.Visible = false;
                    visited[4]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p5.Bounds))
            {
                if (visited[5] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p5.Visible = false;
                    visited[5]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p6.Bounds))
            {
                if (visited[6] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p6.Visible = false;
                    visited[6]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p7.Bounds))
            {
                if (visited[7] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p7.Visible = false;
                    visited[7]++;
                }
            }

            else if (WCKD.Bounds.IntersectsWith(p8.Bounds))
            {
                if (visited[8] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p8.Visible = false;
                    visited[8]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p9.Bounds))
            {
                if (visited[9] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p9.Visible = false;
                    visited[9]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p10.Bounds))
            {
                if (visited[10] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p10.Visible = false;
                    visited[10]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p11.Bounds))
            {
                if (visited[11] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p11.Visible = false;
                    visited[11]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p12.Bounds))
            {
                if (visited[12] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p12.Visible = false;
                    visited[12]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p13.Bounds))
            {
                if (visited[13] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p13.Visible = false;
                    visited[13]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p14.Bounds))
            {
                if (visited[14] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p14.Visible = false;
                    visited[14]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p15.Bounds))
            {
                if (visited[15] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p15.Visible = false;
                    visited[15]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p16.Bounds))
            {
                if (visited[16] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p16.Visible = false;
                    visited[16]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p17.Bounds))
            {
                if (visited[17] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p17.Visible = false;
                    visited[17]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p18.Bounds))
            {
                if (visited[18] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p18.Visible = false;
                    visited[18]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p19.Bounds))
            {
                if (visited[19] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p19.Visible = false;
                    visited[19]++;
                }

            }
            else if (WCKD.Bounds.IntersectsWith(p20.Bounds))
            {

                if (visited[20] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p20.Visible = false;

                    visited[20]++;
                }


            }
            else if (WCKD.Bounds.IntersectsWith(p21.Bounds))
            {
                if (visited[21] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p21.Visible = false;
                    visited[21]++;
                }



            }
            else if (WCKD.Bounds.IntersectsWith(p22.Bounds))
            {
                if (visited[22] == 00)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p22.Visible = false;
                    visited[22]++;
                }


            }
            else if (WCKD.Bounds.IntersectsWith(p23.Bounds))
            {

                if (visited[23] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p23.Visible = false;
                    visited[23]++;
                }

            }
            else if (WCKD.Bounds.IntersectsWith(p24.Bounds))
            {

                if (visited[24] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p24.Visible = false;
                    visited[24]++;
                }
            }
            else if (WCKD.Bounds.IntersectsWith(p25.Bounds))
            {

                if (visited[25] == 0)
                {
                    System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                    string fullPathToSound = @"..\..\..\Sounds\mancare.wav";
                    playerMusic.SoundLocation = fullPathToSound;
                    playerMusic.Load();
                    playerMusic.Play();
                    if (Points.Value <= 190)
                    {
                        Points.Value += 10;
                    }
                    else
                    {
                        lvlComplete = 1;
                    }
                    p25.Visible = false;
                    visited[25]++;
                }
            }
            #endregion
            #region deathIntersection
            else if (WCKD.Bounds.IntersectsWith(e1.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(e2.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(e3.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(e4.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(e5.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(e6.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(e7.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else if (WCKD.Bounds.IntersectsWith(e8.Bounds))
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\GameOver.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                Life.Value = 0;
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            #endregion
            #region finishIntersection
            else if (WCKD.Bounds.IntersectsWith(finish.Bounds) && lvlComplete == 1)
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\Shutdown.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                timer.Stop();
                this.Hide();
                youWonForm open_gamew_form = new youWonForm();
                open_gamew_form.setScore(Points.Value);
                open_gamew_form.setTime(timpRamas.Value);
                open_gamew_form.setHealth(Life.Value);
                open_gamew_form.Closed += (s, args) => this.Close();
                open_gamew_form.Show();
            }
            #endregion
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timeLeft += 1;
            if (timeLeft == 60)
            {
                timer.Stop();
                this.Hide();
                gameOverForm open_game_form = new gameOverForm();
                open_game_form.setScore(Points.Value);
                open_game_form.setTime(timpRamas.Value);
                open_game_form.setHealth(Life.Value);
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();
            }
            else
            {
                timpRamas.Value = 60 - timeLeft;
            }
        }



    }
}

