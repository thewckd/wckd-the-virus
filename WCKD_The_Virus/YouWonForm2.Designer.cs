﻿namespace WCKD_The_Virus
{
    partial class YouWonForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(YouWonForm2));
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.HealthLeft = new System.Windows.Forms.ProgressBar();
            this.TimeLeft = new System.Windows.Forms.ProgressBar();
            this.Score = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Transparent;
            this.button2.Location = new System.Drawing.Point(275, 404);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(147, 148);
            this.button2.TabIndex = 3;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickExit);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Transparent;
            this.button1.Location = new System.Drawing.Point(71, 404);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 148);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickMap);
            // 
            // HealthLeft
            // 
            this.HealthLeft.ForeColor = System.Drawing.Color.LimeGreen;
            this.HealthLeft.Location = new System.Drawing.Point(205, 708);
            this.HealthLeft.Margin = new System.Windows.Forms.Padding(4);
            this.HealthLeft.Name = "HealthLeft";
            this.HealthLeft.Size = new System.Drawing.Size(269, 28);
            this.HealthLeft.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.HealthLeft.TabIndex = 7;
            this.HealthLeft.Value = 80;
            // 
            // TimeLeft
            // 
            this.TimeLeft.ForeColor = System.Drawing.Color.Yellow;
            this.TimeLeft.Location = new System.Drawing.Point(205, 673);
            this.TimeLeft.Margin = new System.Windows.Forms.Padding(4);
            this.TimeLeft.Maximum = 60;
            this.TimeLeft.Name = "TimeLeft";
            this.TimeLeft.Size = new System.Drawing.Size(269, 28);
            this.TimeLeft.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.TimeLeft.TabIndex = 6;
            this.TimeLeft.Value = 40;
            // 
            // Score
            // 
            this.Score.Location = new System.Drawing.Point(205, 637);
            this.Score.Margin = new System.Windows.Forms.Padding(4);
            this.Score.Maximum = 200;
            this.Score.Name = "Score";
            this.Score.Size = new System.Drawing.Size(269, 28);
            this.Score.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Score.TabIndex = 5;
            this.Score.Value = 50;
            // 
            // YouWonForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WCKD_The_Virus.Properties.Resources.youWon1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(487, 769);
            this.Controls.Add(this.HealthLeft);
            this.Controls.Add(this.TimeLeft);
            this.Controls.Add(this.Score);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "YouWonForm2";
            this.Text = "YOU WON!";
            this.ResumeLayout(false);

        }

        public void setScore(int score)
        {
            this.Score.Value = score;
        }
        public void setTime(int time)
        {
            this.TimeLeft.Value = time;
        }

        public void setHealth(int health)
        {
            this.HealthLeft.Value = health;
        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar HealthLeft;
        private System.Windows.Forms.ProgressBar TimeLeft;
        private System.Windows.Forms.ProgressBar Score;
    }
}