﻿namespace WCKD_The_Virus
{
    partial class Maps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Maps));
            this.MSI_map = new System.Windows.Forms.Button();
            this.Back_Button = new System.Windows.Forms.Button();
            this.ASUS_map = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MSI_map
            // 
            this.MSI_map.BackColor = System.Drawing.Color.Transparent;
            this.MSI_map.FlatAppearance.BorderSize = 0;
            this.MSI_map.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.MSI_map.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.MSI_map.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MSI_map.Location = new System.Drawing.Point(16, 15);
            this.MSI_map.Margin = new System.Windows.Forms.Padding(4);
            this.MSI_map.Name = "MSI_map";
            this.MSI_map.Size = new System.Drawing.Size(367, 425);
            this.MSI_map.TabIndex = 0;
            this.MSI_map.UseVisualStyleBackColor = false;
            this.MSI_map.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MSI_map_mouseClick);
            // 
            // Back_Button
            // 
            this.Back_Button.BackColor = System.Drawing.Color.Transparent;
            this.Back_Button.FlatAppearance.BorderSize = 0;
            this.Back_Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Back_Button.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Back_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Back_Button.Location = new System.Drawing.Point(315, 816);
            this.Back_Button.Margin = new System.Windows.Forms.Padding(4);
            this.Back_Button.Name = "Back_Button";
            this.Back_Button.Size = new System.Drawing.Size(151, 121);
            this.Back_Button.TabIndex = 1;
            this.Back_Button.UseVisualStyleBackColor = false;
            this.Back_Button.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Back_Button_MouseClick);
            // 
            // ASUS_map
            // 
            this.ASUS_map.BackColor = System.Drawing.Color.Transparent;
            this.ASUS_map.FlatAppearance.BorderSize = 0;
            this.ASUS_map.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ASUS_map.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ASUS_map.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ASUS_map.Location = new System.Drawing.Point(398, 15);
            this.ASUS_map.Margin = new System.Windows.Forms.Padding(4);
            this.ASUS_map.Name = "ASUS_map";
            this.ASUS_map.Size = new System.Drawing.Size(367, 425);
            this.ASUS_map.TabIndex = 2;
            this.ASUS_map.UseVisualStyleBackColor = false;
            this.ASUS_map.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ASUS_map_mouseClick);
            // 
            // Maps
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WCKD_The_Virus.Properties.Resources.Maps_Wallpaper2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(777, 951);
            this.Controls.Add(this.ASUS_map);
            this.Controls.Add(this.Back_Button);
            this.Controls.Add(this.MSI_map);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Maps";
            this.Text = "Maps";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button MSI_map;
        private System.Windows.Forms.Button Back_Button;
        private System.Windows.Forms.Button ASUS_map;
    }
}