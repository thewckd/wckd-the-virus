﻿namespace WCKD_The_Virus
{
    partial class gameOverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gameOverForm));
            this.Score = new System.Windows.Forms.ProgressBar();
            this.TimeLeft = new System.Windows.Forms.ProgressBar();
            this.HealthLeft = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Score
            // 
            this.Score.Location = new System.Drawing.Point(239, 444);
            this.Score.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Score.Maximum = 200;
            this.Score.Name = "Score";
            this.Score.Size = new System.Drawing.Size(269, 28);
            this.Score.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Score.TabIndex = 0;
            this.Score.Value = 50;
            // 
            // TimeLeft
            // 
            this.TimeLeft.ForeColor = System.Drawing.Color.Yellow;
            this.TimeLeft.Location = new System.Drawing.Point(239, 480);
            this.TimeLeft.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TimeLeft.Maximum = 60;
            this.TimeLeft.Name = "TimeLeft";
            this.TimeLeft.Size = new System.Drawing.Size(269, 28);
            this.TimeLeft.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.TimeLeft.TabIndex = 1;
            this.TimeLeft.Value = 40;
            // 
            // HealthLeft
            // 
            this.HealthLeft.ForeColor = System.Drawing.Color.LimeGreen;
            this.HealthLeft.Location = new System.Drawing.Point(239, 516);
            this.HealthLeft.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.HealthLeft.Name = "HealthLeft";
            this.HealthLeft.Size = new System.Drawing.Size(269, 28);
            this.HealthLeft.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.HealthLeft.TabIndex = 2;
            this.HealthLeft.Value = 80;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(69, 609);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 105);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickRetry);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(225, 609);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(115, 105);
            this.button2.TabIndex = 4;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickMap);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(379, 609);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(129, 105);
            this.button3.TabIndex = 5;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MouseClickExit);
            // 
            // gameOverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WCKD_The_Virus.Properties.Resources.gameover;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(552, 729);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.HealthLeft);
            this.Controls.Add(this.TimeLeft);
            this.Controls.Add(this.Score);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "gameOverForm";
            this.Text = "GAME OVER!";
            this.ResumeLayout(false);

        }

        public void setScore(int score)
        {
            this.Score.Value = score;
        }
        public void setTime(int time)
        {
            this.TimeLeft.Value = time;
        }

        public void setHealth(int health)
        {
            this.HealthLeft.Value = health;
        }


        #endregion

        private System.Windows.Forms.ProgressBar Score;
        private System.Windows.Forms.ProgressBar TimeLeft;
        private System.Windows.Forms.ProgressBar HealthLeft;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}