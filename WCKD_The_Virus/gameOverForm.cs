﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WCKD_The_Virus
{
    public partial class gameOverForm : Form
    {
        public gameOverForm()
        {
            InitializeComponent();
        }
        private void MouseClickRetry(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\button.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                this.Hide();
                Form1 open_game_form = new Form1(); 
                open_game_form.Closed += (s, args) => this.Close();
                open_game_form.Show();

             }

        }

        private void MouseClickMap(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                System.Media.SoundPlayer playerMusic = new System.Media.SoundPlayer();
                string fullPathToSound = @"..\..\..\Sounds\button.wav";
                playerMusic.SoundLocation = fullPathToSound;
                playerMusic.Load();
                playerMusic.Play();
                this.Hide();
                Maps open_game_maps = new Maps();
                open_game_maps.Closed += (s, args) => this.Close();
                open_game_maps.Show();
            }

        }

        private void MouseClickExit(object sender, MouseEventArgs e)
        {
            Application.Exit();
        }
    }
}
